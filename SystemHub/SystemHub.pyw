#!/usr/bin/python
from random import randint, triangular
from threading import Thread
import subprocess
import datetime
import sys

try:
    from PyQt5 import QtWidgets
    from PyQt5.QtWidgets import QFileDialog
    from Data.scripts.theme import Theme
    import Data.scripts.dMain as Main
    import Data.scripts.dAbout as dAbout
    import Data.scripts.dVigenere as dVigenere
    import Data.scripts.dFacts as dFacts
    import Data.scripts.dNumber_system as dNumber_system
    import Data.scripts.dBnh_and_test as dBnh_and_test
    import Data.scripts.dBenchmark as dBenchmark
    import Data.scripts.dComp_test as dComp_test
    import Data.scripts.dDemotivator as dDemotivator
    from PIL import Image, ImageDraw, ImageFont
    from PyQt5.QtGui import *
    from PyQt5.QtCore import *
except ModuleNotFoundError:
    from tkinter import messagebox
    messagebox.showinfo('Внимание!', 'Обнаружены недостающие модули, сейчас они будут установлены')
    subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'pyqt5', 'pillow'])


class SystemHubWindow(QtWidgets.QMainWindow, Main.Ui_MainWindow):  # доступ к переменным из файла
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.About = AboutWindow()
        self.Facts = FactsWindow()
        self.Vigenere = VigenereWindow()
        self.NumberSystem = NumberSystemWindow()
        self.BenchmarkAndTest = BenchmarkAndTestWindow()
        self.Demotivator = DemotivatorWindow()

        self.setStyleSheet('.QWidget {background-image: url(:/newPrefix/main_bg_img.jpeg);}')
        self.Calc_button.clicked.connect(self.Vigenere.show)
        self.pushButton_3.clicked.connect(self.NumberSystem.show)
        self.pushButton_4.clicked.connect(self.Facts.show)
        self.pushButton_5.clicked.connect(self.About.show)
        self.pushButton_2.clicked.connect(self.BenchmarkAndTest.show)
        self.pushButton_6.clicked.connect(self.Demotivator.show)


class AboutWindow(QtWidgets.QMainWindow, dAbout.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton.clicked.connect(lambda: self.close())


class VigenereWindow(QtWidgets.QMainWindow, dVigenere.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setStyleSheet('.QWidget {background-image: url(:/newPrefix/vigenere_bg_img.jpg);}')
        self.pushButton.clicked.connect(self.encrypt)
        self.eng_alphabet = 'abcdefghijklmnopqrstuvwxyz'
        self.rus_alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'

    def encrypt(self):
        key = self.lineEdit.text().replace(' ', '')
        if not key.isalpha():
            key = ''.join(filter(str.isalpha, key))
            self.lineEdit.setText(key)
            if self.lineEdit.text() == '':
                QtWidgets.QMessageBox.critical(self, "NOOOOOOOOO", "Ключ должен состоять из букв!")
                return
        text = self.lineEdit_2.text().split()
        result = ''
        k = 1
        if self.checkBox.isChecked():
            k = -1
        if self.comboBox.currentText() == 'RU':
            alphabet = self.rus_alphabet
        else:
            alphabet = self.eng_alphabet
        key_cnt = 0
        for word in range(len(text)):
            for char in range(len(text[word])):
                if text[word][char].lower() in alphabet:
                    key_pos = alphabet.find(key[key_cnt].lower()) * k
                    if key_cnt + 1 >= len(key):
                        key_cnt = 0
                    else:
                        key_cnt += 1
                    position = alphabet.find(text[word][char].lower())
                    if position + abs(key_pos) >= len(alphabet):
                        position = (position + key_pos) % len(alphabet)
                    else:
                        position += key_pos
                    result += alphabet[position].upper() if text[word][char].isupper() else alphabet[position]
                else:
                    result += text[word][char]
            result += ' '
        self.textEdit.setPlainText(result)


class FactsWindow(QtWidgets.QMainWindow, dFacts.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton.clicked.connect(self.random_fact)

        self.facts_list = {1: "Apple Macintosh и Commodore Amiga 1000 имеют подписи разработчиков, которые они "
                              "закинули внутрь корпусов этих компьютеров.",
                           2: "GRiD 1101 — дедушка всех современных ноутбуков. Он стоил больше $8000 в 1982 году, "
                              "а в 1985 даже побывал в космосе (на шаттле «Discovery»).",
                           3: "Компания Commodore считала плохой идеей выпуск VIC-20, первого дешевого (<$300) "
                              "цветного компьютера. Тем не менее, VIC-20 принес разработчикам сотни миллионов "
                              "долларов, став первым в мире компьютером, продажи которого превысили миллион "
                              "экземпляров.",
                           4: "Первый портативный Macintosh (Macintosh Portable, 1989) имел 16 MHz процессор и весил "
                              "более 7-ми килограммов.",
                           5: "Первым в мире карманным ПК (КПК) под управлением MS-DOS стал компьютер Portfolio, "
                              "который разработали в Atari в 1989 году.",
                           6: "Первый IBM-совместимый персональный компьютер, работающий на батареях, был разработан "
                              "в 1986 году и назывался IBM Convertible PC.",
                           7: "Time Sinclair 1000 — самый успешный в мире компьютер, который продавался менее чем за "
                              "$100.",
                           8: "Согласно книге рекордов Гиннесса, компьютер Commodore 64 (1982) сохраняет звание самой "
                              "популярной компьютерной модели в мире. Его продажи превысили 10 миллионов экземпляров.",
                           9: "Портативная версия Commodore 64 называется SX-64 (1984), имеет встроенный 5-дюймовый "
                              "цветной CRT дисплей и весит 10 килограммов.",
                           10: "Компьютер Coleco Adam (1983) можно запустить только подключив к нему принтер. Причина "
                               "в том, что принтер используется в качестве блока питания для компьютера.",
                           11: "Компьютер Jupiter Ace (1983) смело рекламировался как «Возможно, самый быстрый "
                               "микрокомпьютер во вселенной!».",
                           12: "Apple Computers подали в суд на производителя Franklin ACE 100 (1982) за нарушение "
                               "авторских прав. Дело в том, что компания Franklin изменила пару слов в операционной "
                               "системе Apple II и начала продавать её как свою собственную.",
                           13: "В 1981 году, ссылаясь на известный в те времена ПК, компания IBM продавала настольный "
                               "компьютер под названием Datamaster.",
                           14: "Osborne 1 (1981) считается первым практичным и полезным «портативным» компьютером. Он "
                               "весит более 11-ти килограммов.",
                           15: "Первым великим провалом для Apple стал выпуск компьютера Apple III в 1981.",
                           16: "Radio Shack Pocket Computer 1980 года — первый программируемый компьютер, который "
                               "можно носить в кармане рубашки.",
                           17: "Вероятно, самый тяжелый настольный компьютер в истории это IBM 5120 1980 года — он "
                               "весил 48 килограммов и это без 60-ти килограммового флоппи-дисковода.",
                           18: "Центральный процессор компьютера HP-85 (1980) работал на частоте 0.6 MHz.",
                           19: "В полностью собранном виде компьютер TI-99/4 (1979) компании Texas Instruments может "
                               "быть шириной более 3-х метров.",
                           20: "В 1979 году Apple лицензировала Apple II для продажи в государственные школы. Для "
                               "этого они перекрасили бежевый корпус в черный цвет.",
                           21: "Компания NorthStar, которая разрабатывала и продавала компьютеры в конце 70-x, "
                               "изначально называлась «Kentucky Fried Computers», KFC сокращенно (прим. ред.: "
                               "аналогия с известной сетью кафе KFC, название которой расшифровывается как «Kentucky "
                               "Fried Chicken»).",
                           22: "Считается, что Commodore PET-2001 (1977) имеет худшую клавиатуру, чем любой другой "
                               "полноразмерный компьютер.",
                           23: "Портативный настольный компьютер IBM 5100 (1975) мог стоить до $20 000, в зависимости "
                               "от включенных опций.",
                           24: "Zenith MiniSport (1989) — единственный компьютер, использовавший 2-дюймовый "
                               "флоппи-дисковод.",
                           25: "Apple Lisa (1983) стал первым коммерчески успешным компьютером с графическим "
                               "пользовательским интерфейсом (graphical user interface, GUI) и мышью. Он стоил $10 "
                               "000. "
                           }

        self.last = []

    def random_fact(self):
        rand = randint(1, 25)
        while rand in self.last:
            rand = randint(1, 25)
        if len(self.last) < 10:
            self.last.append(rand)
        else:
            self.last = []
        self.textEdit.setText(self.facts_list.get(rand))


class NumberSystemWindow(QtWidgets.QMainWindow, dNumber_system.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.convertButton.setIconSize(QSize(40, 40))
        self.convertButton.clicked.connect(self.systems)

    def systems(self):
        select_system = [self.firstBase.currentText(), self.secondBase.currentText()]
        enter = self.Entry.text()
        int_base = {'Двоичная': 2, 'Десятичная': 10, 'Восьмеричная': 8, 'Шестнадцатиричная': 16}

        def convert(n, m):
            try:
                dec = int(enter, int_base[n])  # перевод изначального числа в десятичную систему
            except ValueError:
                QtWidgets.QMessageBox.critical(self, "NOOOOOOOOO", "Неверно выбранна система!")
                return ''
            return {'Двоичная': bin(dec)[2:],
                    'Десятичная': dec,
                    'Восьмеричная': oct(dec)[2:],
                    'Шестнадцатиричная': hex(dec)[2:]}.get(m)

        self.Result.setText(str(convert(select_system[0], select_system[1])))


class BenchmarkAndTestWindow(QtWidgets.QMainWindow, dBnh_and_test.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.Benchmark = BenchmarkWindow()
        self.ComputerTest = ComputerTestWindow()

        self.setStyleSheet('.QWidget {background-image: url(:/newPrefix/bnch_and_test.png);}')
        self.pushButton_2.clicked.connect(self.open_test)
        self.pushButton.clicked.connect(self.open_bench)

    def open_bench(self):
        self.close()
        self.Benchmark.show()

    def open_test(self):
        self.close()
        self.ComputerTest.show()


class BenchmarkWindow(QtWidgets.QMainWindow, dBenchmark.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setStyleSheet('.QWidget {background-image: url(:/newPrefix/benchmark_bg.png);}')
        self.pushButton.clicked.connect(lambda: self.start_bench())

    def start_bench(self):

        def bench_one():  # простое число
            self.label.setText('В процессе')
            n = 20000
            l = []

            start = datetime.datetime.now()

            for i in range(2, n + 1):
                cnt = 0
                for j in range(1, i + 1):
                    if i % j == 0:
                        cnt += 1
                if cnt == 2:
                    l.append(i)

            tm = datetime.datetime.now() - start
            self.label.setText(str(tm))
            Thread(target=bench_two).start()

        def bench_two():  # вычисления с плавающей точкой
            self.label_2.setText('В процессе')
            start = datetime.datetime.now()
            n = 0.0
            for i in range(10 ** 7):
                n += (triangular(-10 ** 30, 10 ** 30) / triangular(-10 ** 30, 10 ** 30)) - (
                        triangular(-10 ** 30, 10 ** 30) + triangular(-10 ** 30, 10 ** 30))

            tm = datetime.datetime.now() - start
            self.label_2.setText(str(tm))
            Thread(target=bench_three()).start()

        def bench_three():  # проверка оперативной памяти
            self.label_3.setText('В процессе')
            start = datetime.datetime.now()
            d = {i: i for i in range(100000000)}
            tm = datetime.datetime.now() - start
            self.label_3.setText(str(tm))

        Thread(target=bench_one).start()


class ComputerTestWindow(QtWidgets.QMainWindow, dComp_test.Ui_MainWindow):

    def start_test(self):
        def select(num_quest, order=0, q_list=0):
            self.tabWidget.currentWidget().setDisabled(1)
            answer = self.sender()
            print(type(answer))
            if str(type(answer)) == "<class 'PyQt5.QtWidgets.QRadioButton'>":
                self.enter_answers[num_quest] = answer.text()
                self.count_enter_answers += 1
            elif str(type(answer)) == "<class 'PyQt5.QtWidgets.QPushButton'>":
                print('Button!')
                if len(self.temp[num_quest]) != 0:
                    self.enter_answers[num_quest] = self.temp[num_quest]
                    self.count_enter_answers += 1
                else:
                    QtWidgets.QMessageBox.critical(self, 'Ошибка', 'Выбрано слишком мало вариантов')
                    self.tabWidget.currentWidget().setDisabled(0)
            else:
                if str(type(answer)) == "<class 'PyQt5.QtWidgets.QComboBox'>":
                    q_list[order] = answer.currentText()
                else:
                    q_list[order] = answer.text() if answer.isChecked() else 0
                self.temp[num_quest] = q_list
                self.tabWidget.currentWidget().setDisabled(0)
            if self.count_enter_answers == 12:
                self.test_over()

        self.radioButton.toggled.connect(lambda: select(1))
        self.radioButton_2.toggled.connect(lambda: select(1))
        self.radioButton_3.toggled.connect(lambda: select(1))

        question_2_list = [0] * 4
        self.checkBox.stateChanged.connect(lambda: select(2, 0, question_2_list))
        self.checkBox_2.stateChanged.connect(lambda: select(2, 1, question_2_list))
        self.checkBox_3.stateChanged.connect(lambda: select(2, 2, question_2_list))
        self.checkBox_4.stateChanged.connect(lambda: select(2, 3, question_2_list))
        self.question_2_btn.clicked.connect(lambda: select(2, 4, question_2_list))

        question_3_list = ['Операционные системы'] * 4
        self.comboBox.currentIndexChanged.connect(lambda: select(3, 0, question_3_list))
        self.comboBox_2.currentIndexChanged.connect(lambda: select(3, 1, question_3_list))
        self.comboBox_3.currentIndexChanged.connect(lambda: select(3, 2, question_3_list))
        self.comboBox_4.currentIndexChanged.connect(lambda: select(3, 3, question_3_list))
        self.question_3_btn.clicked.connect(lambda: select(3))

        self.radioButton_4.toggled.connect(lambda: select(4))
        self.radioButton_5.toggled.connect(lambda: select(4))
        self.radioButton_6.toggled.connect(lambda: select(4))
        self.radioButton_7.toggled.connect(lambda: select(4))

        self.radioButton_8.toggled.connect(lambda: select(5))
        self.radioButton_9.toggled.connect(lambda: select(5))
        self.radioButton_10.toggled.connect(lambda: select(5))

        self.radioButton_11.toggled.connect(lambda: select(6))
        self.radioButton_12.toggled.connect(lambda: select(6))
        self.radioButton_13.toggled.connect(lambda: select(6))

        self.radioButton_14.toggled.connect(lambda: select(7))
        self.radioButton_15.toggled.connect(lambda: select(7))
        self.radioButton_16.toggled.connect(lambda: select(7))

        question_8_list = ['ОС возвращается в состояние ожидания'] * 4
        self.comboBox_5.currentIndexChanged.connect(lambda: select(8, 0, question_8_list))
        self.comboBox_6.currentIndexChanged.connect(lambda: select(8, 1, question_8_list))
        self.comboBox_7.currentIndexChanged.connect(lambda: select(8, 2, question_8_list))
        self.comboBox_8.currentIndexChanged.connect(lambda: select(8, 3, question_8_list))
        self.question_8_btn.clicked.connect(lambda: select(8))

        self.radioButton_17.toggled.connect(lambda: select(9))
        self.radioButton_18.toggled.connect(lambda: select(9))
        self.radioButton_19.toggled.connect(lambda: select(9))

        self.radioButton_20.toggled.connect(lambda: select(10))
        self.radioButton_21.toggled.connect(lambda: select(10))
        self.radioButton_22.toggled.connect(lambda: select(10))
        self.radioButton_23.toggled.connect(lambda: select(10))

        self.radioButton_24.toggled.connect(lambda: select(11))
        self.radioButton_25.toggled.connect(lambda: select(11))
        self.radioButton_26.toggled.connect(lambda: select(11))

        self.radioButton_27.toggled.connect(lambda: select(12))
        self.radioButton_28.toggled.connect(lambda: select(12))
        self.radioButton_29.toggled.connect(lambda: select(12))
        self.radioButton_30.toggled.connect(lambda: select(12))

    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.temp = {i: [] for i in range(1, 13)}
        self.count_enter_answers = 0
        self.enter_answers = {i: [] for i in range(1, 13)}
        self.correct_answers = {
            1: 'совокупность программ, хранящихся в\nдолговременной памяти компьютера.',
            2: ['прикладные программы', 0, 'системные программы', 'системы программирования'],
            3: ['Операционные системы', 'Табличные процессоры', 'Бухгалтерские программы', 'Паскаль'],
            4: 'Visual Studio Code',
            5: 'комплекс программ, обеспечивающих\nсогласованную работу всех аппаратных средств\nкомпьютера и выполняемых программ',
            6: 'утилита',
            7: 'драйверы',
            8: ['ОС находится в состоянии ожидания команды пользователя', 'пользователь дает команду',
                'ОС исполняет команду или сообщает о невозможности выполнения', 'ОС возвращается в состояние ожидания'],
            9: 'Фоновый процесс запускаемый при системе,\nкоторый продолжает работать в виде служб',
            10: 'Системный загрузчик',
            11: 'Иерархическую структуру',
            12: 'PHP'
        }
        self.start_test()

    def test_over(self):
        self.tabWidget.setCurrentIndex(12)
        cnt = 12
        for i in range(13):
            if self.enter_answers.get(i) != self.correct_answers.get(i):
                cnt -= 1
        if cnt > 8:
            self.label_20.setText("Вы прошли тест!!")
            QtWidgets.QMessageBox.information(self, "Юху", "Вы прошли тест!!")
        else:
            self.label_20.setText("Вы завалили тест!!")
            QtWidgets.QMessageBox.information(self, "Юху", "Вы завалили тест!!")
        self.label_21.setText(f'{int(100 / (12 / cnt))}%')


class DemotivatorWindow(QtWidgets.QMainWindow, dDemotivator.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton_2.clicked.connect(self.open_user_img)
        self.pushButton.setDisabled(1)
        self.pushButton.clicked.connect(self.create_dem)
        self.user_img = None

    def open_user_img(self):
        try:
            img_path = QFileDialog.getOpenFileName()[0]
            print(img_path)
            self.user_img = Image.open(img_path).resize((366, 386), Image.ANTIALIAS)
        except:
            return
        self.pushButton_2.setText('✓')
        self.pushButton.setDisabled(0)

    def create_dem(self):
        dem_img = Image.open('Data/resurces/dem.png')
        i_draw = ImageDraw.Draw(dem_img)
        text_dem = self.lineEdit.text()
        font = ImageFont.truetype('Data/resurces/raleway-black.ttf', encoding="unic", size=30)
        i_draw.text((220, 473), text_dem, font=font, anchor='ms')
        dem_img.paste(self.user_img, (39, 39))

        try:
            path_save = QFileDialog.getSaveFileName()[0]
            print(path_save)
            dem_img.save(f'{path_save}.png')
        except:
            return


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = SystemHubWindow()
    Theme(app)
    window.show()
    app.exec()


if __name__ == '__main__':
    main()
