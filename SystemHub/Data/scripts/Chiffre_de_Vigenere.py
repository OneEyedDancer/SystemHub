#!/usr/bin/python
import sys
from PyQt5 import QtWidgets
from theme import Theme
import dVigenere

eng_alphabet = 'abcdefghijklmnopqrstuvwxyz'
rus_alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'


class App(QtWidgets.QMainWindow, dVigenere.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setStyleSheet('.QWidget {background-image: url(:/newPrefix/vigenere_bg_img.jpg);}')
        self.pushButton.clicked.connect(self.encrypt)

    def encrypt(self):
        key = self.lineEdit.text().replace(' ', '')
        if not key.isalpha():
            key = ''.join(filter(str.isalpha, key))
            self.lineEdit.setText(key)
            if self.lineEdit.text() == '':
                QtWidgets.QMessageBox.critical(self, "NOOOOOOOOO", "Ключ должен состоять из букв!")
                return
        text = self.lineEdit_2.text().split()
        result = ''
        k = 1
        if self.checkBox.isChecked():
            k = -1
        if self.comboBox.currentText() == 'RU':
            alphabet = rus_alphabet
        else:
            alphabet = eng_alphabet
        key_cnt = 0
        for word in range(len(text)):
            for char in range(len(text[word])):
                if text[word][char].lower() in alphabet:
                    key_pos = alphabet.find(key[key_cnt].lower()) * k
                    if key_cnt + 1 >= len(key):
                        key_cnt = 0
                    else:
                        key_cnt += 1
                    position = alphabet.find(text[word][char].lower())
                    if position + abs(key_pos) >= len(alphabet):
                        position = (position + key_pos) % len(alphabet)
                    else:
                        position += key_pos
                    result += alphabet[position].upper() if text[word][char].isupper() else alphabet[position]
                else:
                    result += text[word][char]
            result += ' '
        self.textEdit.setPlainText(result)


app = QtWidgets.QApplication(sys.argv)
window = App()
Theme(app)
window.show()
app.exec()